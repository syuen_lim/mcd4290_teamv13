"use strict";
mapboxgl.accessToken = "pk.eyJ1IjoiY2h1bm1hbmxvIiwiYSI6ImNrb2Zmcno3YzA0eXYyb3A3Y2RxYnlscWsifQ.kKJs5PgfSGNsuTGOjvXONQ";

let countryData = ["Afghanistan", "Albania", "Algeria", "American Samoa", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "British Indian Ocean Territory", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burma", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo (Brazzaville)", "Congo (Kinshasa)", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands", "Faroe Islands", "Fiji", "Finland", "France", "French Guiana", "French Polynesia", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Johnston Atoll", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Lithuania", "Luxembourg", "Macau", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia", "Midway Islands", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "North Korea", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Palestine", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russia", "Rwanda", "Saint Helena", "Saint Kitts and Nevis", "Saint Lucia", "Saint Pierre and Miquelon", "Saint Vincent and the Grenadines", "Samoa", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the Islands", "South Korea", "South Sudan", "Spain", "Sri Lanka", "Sudan", "Suriname", "Svalbard", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Togo", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands", "Wake Island", "Wallis and Futuna", "West Bank", "Western Sahara", "Yemen", "Zambia", "Zimbabwe"];

const endpoint_allRoutes = "https://eng1003.monash/OpenFlights/allroutes/";
const endpoint_allAirports = "https://eng1003.monash/OpenFlights/airports/";
const endpoint_routesFromAirport = "https://eng1003.monash/OpenFlights/routes/";

const key_airportsInSelectedCountry = "TS_AirportsInSelectedCountry";
const key_allRoutesInSelectedCountry = "TS_AllRoutesInSelectedCountry";
const key_currentTrip = "TS_CurrentTrip";
const key_user = "TS_User";
const key_tripToViewId = "TS_TripToViewId";

const trip_historical = "Historical";
const trip_future = 'Future';
const trip_all = "All";


function webServiceRequest(url, data) {
    // Build URL parameters from data object.
    let params = "";
    // For each key in data object...
    for (let key in data) {
        if (data.hasOwnProperty(key)) {

            if (params.length == 0) {
                // First parameter starts with '?'
                params += "?";
            }
            else {
                // Subsequent parameter separated by '&'
                params += "&";
            }

            // let encodedKey = encodeURIComponent(key);
            // let encodedValue = encodeURIComponent(data[key]);

            // params += encodedKey + "=" + encodedValue;
            params += key + "=" + data[key];
        }
    }

    let script = document.createElement('script');
    script.src = url + params;
    document.body.appendChild(script);
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');
}

class Airport {
    constructor(airportId, name, country, latitude, longitude) {
        this.airportId = airportId;
        this.name = name;
        this.country = country;
        this.latitude = latitude;
        this.longitude = longitude;
    }
}

class Trip {
    constructor() {
        this.tripId = 0;
        this.startDateTime = new Date();
        //contains a list of airports and each adjacent pair represents a route
        this.routes = [];
    }

    getTotalDistance() {
        let total = 0;
        if (this.routes.length > 1) {

            for (let i = 0; i < this.routes.length - 1; i++) {
                // console.log(this.routes[i])
                // console.log(this.routes[i + 1])
                let distance = calculateDistance(this.routes[i].latitude, this.routes[i].longitude, this.routes[i + 1].latitude, this.routes[i + 1].longitude);
                total += distance;
            }
        }

        return total.toFixed(2);
    }



    getAllRoutes() {
        return this.routes;
    }

    getLastAirport() {
        return this.routes[this.routes.length - 1];
    }

    getNumberOfStops() {
        return this.routes.length;
    }

    addRoute(airport) {
        this.routes.push(airport);
    }

    removeAllRoutes() {
        this.routes = [];
    }

    removeLastRoute() {
        return this.routes.pop();
    }

    fromData(data) {
        let trip = new Trip();
        trip.routes = data.routes;
        trip.startDateTime = data.startDateTime;
        trip.tripId = data.tripId;
        return trip;
    }
}

class User {
    constructor(userId) {
        this.userId = userId;
        this.trips = [];
    }

    getNextTripId() {
        if (this.trips.length > 0) {
            return parseInt(this.trips[this.trips.length - 1].tripId) + 1;
        } else {
            return 0;
        }
    }

    getNumberOfTrips() {
        return this.trips.length;
    }

    getAllTrips() {
        return this.trips;
    }

    addTrip(trip) {
        this.trips.push(trip);
    }

    removeTrip(tripId) {
        let tripToRemove = this.trips.findIndex(t => t.tripId == tripId);
        if (tripToRemove != undefined) {
            this.trips.splice(tripToRemove, 1);
        }
    }

    fromData(data) {
        let user = new User();
        let trips = []
        data.trips.forEach(element => {
            trips.push(new Trip().fromData(element));
        });
        user.trips = trips;
        return user;
    }
}

function calculateDistance(lat1, long1, lat2, long2) {

    //radians
    lat1 = (lat1 * 2.0 * Math.PI) / 60.0 / 360.0;
    long1 = (long1 * 2.0 * Math.PI) / 60.0 / 360.0;
    lat2 = (lat2 * 2.0 * Math.PI) / 60.0 / 360.0;
    long2 = (long2 * 2.0 * Math.PI) / 60.0 / 360.0;


    // use to different earth axis length    
    var a = 6378137.0;        // Earth Major Axis (WGS84)    
    var b = 6356752.3142;     // Minor Axis    
    var f = (a - b) / a;        // "Flattening"    
    var e = 2.0 * f - f * f;      // "Eccentricity"      

    var beta = (a / Math.sqrt(1.0 - e * Math.sin(lat1) * Math.sin(lat1)));
    var cos = Math.cos(lat1);
    var x = beta * cos * Math.cos(long1);
    var y = beta * cos * Math.sin(long1);
    var z = beta * (1 - e) * Math.sin(lat1);

    beta = (a / Math.sqrt(1.0 - e * Math.sin(lat2) * Math.sin(lat2)));
    cos = Math.cos(lat2);
    x -= (beta * cos * Math.cos(long2));
    y -= (beta * cos * Math.sin(long2));
    z -= (beta * (1 - e) * Math.sin(lat2));

    // /1000
    return (Math.sqrt((x * x) + (y * y) + (z * z)) / 20);
}

function updateLocalStorage(key, data) {
    localStorage.setItem(key, JSON.stringify(data));
}

function tomorrow() {
    const today = new Date()
    const tomorrow = new Date(today)
    tomorrow.setDate(tomorrow.getDate() + 1)
    return tomorrow;
}

function generateUniqueUserId() {
    return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
}

//Code run on page load 


let user = new User(generateUniqueUserId());
let currentTrip = new Trip();
localStorage.setItem(key_currentTrip, JSON.stringify(currentTrip));
if (localStorage.getItem(key_user) == undefined) {
    localStorage.setItem(key_user, JSON.stringify(user));
} else {
    user = user.fromData(JSON.parse(localStorage.getItem(key_user)));
}


