"use strict";

let map1 = new mapboxgl.Map({
    container: "map1",
    style: "mapbox://styles/mapbox/streets-v11",
    zoom: 4,
    center: [145.11955, -37.92621]
});


function displayDetail(trip) {
    let allRoutes = trip.getAllRoutes();
    let routes = allRoutes.map(a => a.name).join(" => ");
    let html = `
        <tbody>
            <tr>
                <td class="mdl-data-table__cell--non-numeric">Scheduled Date</td>
                <td class="mdl-data-table__cell--non-numeric">${formatDate(trip.startDateTime)}</td>
            </tr>
            <tr>
                <td class="mdl-data-table__cell--non-numeric">Total Distance</td>
                <td class="mdl-data-table__cell--non-numeric">${trip.getTotalDistance()} km</td>
            </tr>
            <tr>
                <td class="mdl-data-table__cell--non-numeric">Origin Airport</td>
                <td class="mdl-data-table__cell--non-numeric">${allRoutes[0].name}</td>
            </tr>
            <tr>
                <td class="mdl-data-table__cell--non-numeric">Destination Airport</td>
                <td class="mdl-data-table__cell--non-numeric">${allRoutes[allRoutes.length -1].name}</td>
             </tr>
            <tr>
                <td class="mdl-data-table__cell--non-numeric">Routes</td>
                <td class="mdl-data-table__cell--non-numeric">${routes}</td>
            </tr>
            <tr>
                <td class="mdl-data-table__cell--non-numeric">Number of Stops</td>
                <td class="mdl-data-table__cell--non-numeric">${trip.getNumberOfStops()} stops</td>
            </tr>
        </tbody>    
    `;

    document.getElementById("tripDetail").innerHTML = html;

}

function drawRoute(coordinates) {
    if (typeof map1.getLayer('routeToView') !== 'undefined') {
        // Remove map layer & source.
        map1.removeLayer('routeToView').removeSource('routeToView');
    }

    map1.addSource('routeToView', {
        'type': 'geojson',
        'data': {
            'type': 'Feature',
            'properties': {},
            'geometry': {
                'type': 'LineString',
                'coordinates': coordinates
            }
        }
    }
    );

    map1.addLayer({
        'id': 'routeToView',
        'type': 'line',
        'source': 'routeToView',
        'layout': {
            'line-join': 'round',
            'line-cap': 'round'
        },
        'paint': {
            'line-color': "#FF0020",
            'line-width': 5
        }
    });
}



//code run on page load
let tripId = localStorage.getItem(key_tripToViewId);
let trip = user.getAllTrips().find(t => t.tripId == tripId);
displayDetail(trip);

map1.on('load', function () {
    let routes = trip.getAllRoutes();
    let coordinates = routes.map(a => [a.longitude, a.latitude]);

    map1.flyTo({
        center: coordinates[0],
        essential: true // this animation is considered essential with respect to prefers-reduced-motion
    });
   
    drawRoute(coordinates);

    routes.forEach(element => {
        let popup = new mapboxgl.Popup({ closeOnClick: true })
            .setLngLat([element.longitude, element.latitude])
            .setText(element.name)
            .addTo(map1);

        let marker = new mapboxgl.Marker()
            .setLngLat([element.longitude, element.latitude])
            .setPopup(popup)
            .addTo(map1);
    });

});

if(new Date(trip.startDateTime) <= new Date()){
    document.getElementById("btnDeleteTrip").disabled = true;
}

document.getElementById("btnDeleteTrip").addEventListener('click', function (e) {
    if (confirm("Do you really want to delete this trip? ")) {
        user.removeTrip(tripId);
        updateLocalStorage(key_user, user);
        window.location = "scheduledTrips.html";
    }
});
