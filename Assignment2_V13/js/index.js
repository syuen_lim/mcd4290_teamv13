"use strict";
let allRoutesFromAirport = [];

let map = new mapboxgl.Map({
    container: "map",
    style: "mapbox://styles/mapbox/streets-v11",
    zoom: 4,
    center: [134.092090, -26.083647]
});

let features = [];

let geojsonMarkers = {
    'type': 'geojson',
    'data': {
        'type': 'FeatureCollection',
        'features': features
    }
};

let routesFeatures = []

let geojsonRoutes = {
    'type': 'geojson',
    'data': {
        'type': 'FeatureCollection',
        'features': routesFeatures
    }
};

let selectedAirports = []

let geojsonSelectedRoutes = {
    'type': 'geojson',
    'data': {
        'type': 'Feature',
        'properties': {},
        'geometry': {
            'type': 'LineString',
            'coordinates': selectedAirports
        }
    }
}



//add an event listener to date picker
document.getElementById("datePicker").addEventListener("change", function (e) {
    let date = document.getElementById("datePicker").value;
    currentTrip.startDateTime = new Date(date);
    updateLocalStorage(key_currentTrip, currentTrip);
});

document.getElementById("btnSearch").addEventListener("click", function (e) {
    let country = document.getElementById("lstCountry").value;
    if (country != "" && country != undefined) {
        getAllAirportsByCountry(country);
        getAllRoutesByCountry(country);
    } else {
        alert("Please select a country to start searching.")
    }
    map.zoom = 4;
});

function getAllRoutesByAirport(airport) {
    webServiceRequest(endpoint_routesFromAirport, { "sourceAirport": airport, "callback": "onGetAllRoutesByAirportResponse" });
}

function onGetAllRoutesByAirportResponse(response) {
    routesFeatures = [];
    allRoutesFromAirport = response;

    let allRoutesInSelectedCountry = JSON.parse(localStorage.getItem(key_airportsInSelectedCountry));
    response.forEach(route => {
        let sourceAirport = allRoutesInSelectedCountry.find(r => r.airportId == route.sourceAirportId);
        let destinationAirport = allRoutesInSelectedCountry.find(r => r.airportId == route.destinationAirportId);
        //international airports are not considered
        if (destinationAirport != undefined && sourceAirport != undefined) {
            let feature = {
                'type': 'Feature',
                'properties': {},
                'geometry': {
                    'type': 'LineString',
                    'coordinates': [
                        [parseFloat(sourceAirport.longitude), parseFloat(sourceAirport.latitude)],
                        [parseFloat(destinationAirport.longitude), parseFloat(destinationAirport.latitude)]
                    ]
                }
            };
            routesFeatures.push(feature);
        }
    });
    geojsonRoutes.data.features = routesFeatures;
    drawAvailableRoutsFromAirport(geojsonRoutes);

}



function getAllRoutesByCountry(country) {
    webServiceRequest(endpoint_allRoutes, { "country": country, "callback": "onGetAllRoutesByCountryResponse" });
}

function onGetAllRoutesByCountryResponse(response) {
    localStorage.setItem(key_allRoutesInSelectedCountry, JSON.stringify(response));
}

function getAllAirportsByCountry(country) {
    webServiceRequest(endpoint_allAirports, { "country": country, "callback": "onGetAllAirportsByCountryResponse" });
}

function onGetAllAirportsByCountryResponse(response) {
    features = [];
    localStorage.setItem(key_airportsInSelectedCountry, JSON.stringify(response));

    response.forEach(airport => {
        let feature = {
            type: "Feature",
            properties: {
                description: airport.name,
                iconSize: [25, 25],
                airport: airport
            },
            geometry: {
                type: "Point",
                coordinates: [parseFloat(airport.longitude), parseFloat(airport.latitude)]
            }
        }
        features.push(feature);
    });
    geojsonMarkers.data.features = features;

    if (features.length > 0) {
        map.flyTo({
            center: features[0].geometry.coordinates,
            essential: true // this animation is considered essential with respect to prefers-reduced-motion
        });
    }

    drawMarkers(geojsonMarkers);
}



map.on('click', 'markers', function (e) {
    var coordinates = e.features[0].geometry.coordinates.slice();
    var description = e.features[0].properties.description;
    var airport = JSON.parse(e.features[0].properties.airport);

    // Ensure that if the map is zoomed out such that multiple
    // copies of the feature are visible, the popup appears
    // over the copy being pointed to.
    while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
        coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
    }

    new mapboxgl.Popup()
        .setLngLat(coordinates)
        .setHTML(description)
        .addTo(map);

    getAllRoutesByAirport(airport.airportId);

    //Only show menu when there is a route between the last airport in trip and the clicked airport 
    setTimeout(function () {
        if ((currentTrip.getNumberOfStops() == 0 && isSourceAiport(allRoutesFromAirport)) || hasRouteInBetween(currentTrip.getLastAirport(), airport)) {
            setTimeout(function () {
                let choice = showMenu();

                if (['1', '2', '3', '4'].includes(choice)) {

                    if (choice == 1 && currentTrip.getNumberOfStops() > 0) {
                        if (confirm("This will clear the current trip and start setting the trip from beginning as there is a source ariport in the current trip already. Do you want to continue?")) {
                            currentTrip.removeAllRoutes();
                            selectedAirports = [];
                        }
                    }

                    if (choice != 4) {
                        if (currentTrip.getNumberOfStops() == 0 || currentTrip.getLastAirport().airportId != airport.airportId) {
                            currentTrip.addRoute(new Airport(airport.airportId, airport.name, airport.country, airport.latitude, airport.longitude));
                            selectedAirports.push([airport.longitude, airport.latitude]);
                        }

                    } else {
                        if (currentTrip.getNumberOfStops() > 0) {
                            let lastAirport = currentTrip.getLastAirport();
                            if (lastAirport.airportId == airport.airportId) {
                                currentTrip.removeLastRoute();
                                selectedAirports.pop();
                            } else {
                                alert("Only last route can be removed from the trip. ");
                            }
                        } else {
                            alert("Nothing is removed as there is no stop in the current trip yet.");
                        }

                    }

                    geojsonSelectedRoutes.data.geometry.coordinates = selectedAirports;
                    drawSelectedRoute(geojsonSelectedRoutes);

                    if (choice == 3) {
                        if (confirm(`You have scheduled a trip starting on ${currentTrip.startDateTime}. This trip starts from ${currentTrip.routes[0].name} and stops at ${currentTrip.routes[currentTrip.getNumberOfStops() - 1].name}. Would you like to save this trip?`)) {
                            currentTrip.tripId = user.getNextTripId();
                            user.addTrip(currentTrip);
                            updateLocalStorage(key_user, user);
                            localStorage.setItem(key_tripToViewId, currentTrip.tripId);
                            window.location = "tripDetail.html";
                        }
                        else {
                            updateLocalStorage(key_currentTrip, new Trip());
                            clearSelecteRoute();
                        }
                    }
                }
            }, 400);
        }
    }, 400);
});

function isSourceAiport(routes) {
    if (routes != undefined && routes.length != 0) {
        return true;
    }
    return false;
}

function hasRouteInBetween(sourceAiport, destinationAirport) {
    if (sourceAiport == undefined || destinationAirport == undefined) {
        return false;
    }
    else if (sourceAiport.airportId == destinationAirport.airportId) {
        return true;
    } else {
        let allRoutesInSelectedCountry = JSON.parse(localStorage.getItem(key_allRoutesInSelectedCountry));
        let route = allRoutesInSelectedCountry.find(r => r.sourceAirportId == sourceAiport.airportId && r.destinationAirportId == destinationAirport.airportId);

        if (route != undefined) {
            return true;
        }
    }

    return false;
}

function showMenu() {
    let option1 = " 1. Set as source airport\n";
    let option2 = " 2. Set as a stop \n";
    let option3 = " 3. Set as destination airport \n";
    let option4 = " 4. Remove from current trip\n";
    let option = " Your choice: ";
    let choice = prompt(option1 + option2 + option3 + option4 + option);
    return choice;
}

function drawAvailableRoutsFromAirport(geojson) {
    if (typeof map.getLayer('routes') !== 'undefined') {
        map.removeLayer('routes').removeSource('routes');
    }

    map.addSource('routes', geojson);

    map.addLayer({
        'id': 'routes',
        'type': 'line',
        'source': 'routes',
        'layout': {
            'line-join': 'round',
            'line-cap': 'round'
        },
        'paint': {
            'line-color': "#888",
            'line-width': 3
        }
    });
}

function drawMarkers(geojson) {
    if (typeof map.getLayer('markers') !== 'undefined') {
        map.removeLayer('markers').removeSource('markers');
    }

    map.addSource('markers', geojson);

    map.addLayer({
        'id': 'markers',
        'type': 'circle',
        'source': 'markers',
        'paint': {
            'circle-color': '#4264fb',
            'circle-radius': 10,
            'circle-stroke-width': 2,
            'circle-stroke-color': '#ffffff'
        }
    });
}

function clearSelecteRoute() {
    if (typeof map.getLayer('selectedRoute') !== 'undefined') {
        // Remove map layer & source.
        map.removeLayer('selectedRoute').removeSource('selectedRoute');
    }
}

function drawSelectedRoute(geojson) {
    if (typeof map.getLayer('selectedRoute') !== 'undefined') {
        // Remove map layer & source.
        map.removeLayer('selectedRoute').removeSource('selectedRoute');
    }

    //Highlight selected route
    map.addSource('selectedRoute', geojson);

    map.addLayer({
        'id': 'selectedRoute',
        'type': 'line',
        'source': 'selectedRoute',
        'layout': {
            'line-join': 'round',
            'line-cap': 'round'
        },
        'paint': {
            'line-color': "#FF0000",
            'line-width': 5
        }
    });



    if (typeof map.getLayer('markersSelectedAirports') !== 'undefined') {
        map.removeLayer('markersSelectedAirports').removeSource('markersSelectedAirports');
    }

    //Highligt selected Airports

    let featuresSelectedAirports = [];

    geojson.data.geometry.coordinates.forEach(airport => {
        let feature = {
            type: "Feature",
            properties: {},
            geometry: {
                type: "Point",
                coordinates: airport
            }
        }
        featuresSelectedAirports.push(feature);
    });

    let geojsonSelectedAiports = {
        'type': 'geojson',
        'data': {
            'type': 'FeatureCollection',
            'features': featuresSelectedAirports
        }
    }
    map.addSource('markersSelectedAirports', geojsonSelectedAiports);

    map.addLayer({
        'id': 'markersSelectedAirports',
        'type': 'circle',
        'source': 'markersSelectedAirports',
        'paint': {
            'circle-color': '#fc035e',
            'circle-radius': 12,
            'circle-stroke-width': 2,
            'circle-stroke-color': '#ffffff'
        }
    });
}

function populateCountryList() {
    let html = "";
    countryData.forEach(country => {
        html += `<option value="${country}">`;
    });
    document.getElementById("countryList").innerHTML = html;
}

//Code on page load
//initialize the country list
populateCountryList();
//Set the default date for the date picker to be today
document.getElementById("datePicker").value = formatDate(new Date());
//Restrict minimum value for the date picker
document.getElementById("datePicker").setAttribute("min", formatDate(new Date()));







