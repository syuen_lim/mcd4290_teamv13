


if (document.querySelector('input[name="tripOptionGroup"]')) {
    document.querySelectorAll('input[name="tripOptionGroup"]').forEach((elem) => {
        elem.addEventListener("click", function (event) {

            let selection = document.querySelector('input[name="tripOptionGroup"]:checked').value;
            displayTrips(selection);

        });
    });
}


function displayTrips(tripType) {

    let trips = user.getAllTrips();
    if (tripType == 'Historical') {
        trips = trips.filter(t => new Date(t.startDateTime) < new Date());
    }

    if (tripType == 'Future') {
        trips = trips.filter(t => new Date(t.startDateTime) >= new Date());
    }

    let html = "";
    if (Array.isArray(trips)) {
        trips.forEach(trip => {
            let routes = trip.getAllRoutes().map(a => a.name).join(" => ");
            let canBeCancelled = new Date(trip.startDateTime) > new Date();
            html += `
                <tr>
                    <td class="mdl-data-table__cell--non-numeric">${formatDate(trip.startDateTime)}</td>
                    <td class="mdl-data-table__cell--non-numeric">${routes}</td>    
                    <td class="mdl-data-table__cell--non-numeric">${trip.getTotalDistance()} km</td>
                    <td class="mdl-data-table__cell--non-numeric">${trip.getNumberOfStops()} stops</td>                          
                    <td class="mdl-data-table__cell--non-numeric"><a href="#" onclick=View(${trip.tripId})>View Details</a></td>
                    ${canBeCancelled ? `<td class="mdl-data-table__cell--non-numeric"><a href="#" onclick=Cancel(${trip.tripId})>Cancel</a></td>` : `<td class="mdl-data-table__cell--non-numeric"></td>`}
                </tr>`;
        })
    }
    document.getElementById("trips").innerHTML = html;
}

function View(tripId) {
    window.location = "tripDetail.html";
    localStorage.setItem(key_tripToViewId, tripId);
}

function Cancel(tripId) {
    if (confirm("Do you want to cancel this trip?")) {
        // console.log(tripId);
        user.removeTrip(tripId);
        updateLocalStorage(key_user, user);
        displayTrips(trip_all);
    }
}

//code run on page load
displayTrips(trip_all);